const http = require( 'http' );
const { v4: uui } = require('uuid');
const fs = require( 'fs' );


const port = 9998;



const promiseFileHandling = ( path ) =>{
    
    return new Promise ((res , rej) =>{
        
        fs.readFile( path , (error , data) =>{
            
            if( error ){
                
                rej( new Error("Internal Error") );
            } else {
                
                res(data);
            }
        })
    })
};

const Handlinganerror = (req , resp , error) =>{

    resp.writeHead( 500 , {"Content-Type":"text/html"});
    resp.write("Internal Error");
    resp.end();
}

const server = http.createServer((req , resp) =>{
    
    let url = req.url.split('/');               

    switch ( url [ 2 ] ){     

        case "HTML" : {

            promiseFileHandling("../index.html")
            .then((data) =>{
            
                resp.writeHead( 200 , {'Content-Type' : 'text/html',});
                resp.write(data);
                resp.end();
            
            })
            .catch(error =>{

                Handlinganerror( req , resp ,error);
            })

            break;
        }


        case "index.css" : {

            promiseFileHandling("index.css")
            .then((data) =>{
            
                resp.writeHead( 200 , {'Content-Type' : 'text/css',});
                resp.write(data);
                resp.end();
            
            })
            .catch(error =>{
            
                Handlinganerror( req , resp ,error);
            })
            
            break;
        }


        case "JSON" : {

            promiseFileHandling("files.json")
            .then((data) =>{
            
                resp.writeHead( 200 , {'Content-Type' : 'application/json',});
                resp.write(data);
                resp.end();
            
            })
            .catch(error =>{
            
                Handlinganerror( req , resp ,error);
            })

            break;
        }

        
        case "UUID" : {
        
            try{        

                resp.writeHead( 200 , {'Content-Type' : 'application/json'});
                resp.write(JSON.stringify({ "UUID" : uui() } , null , " "));
                resp.end();
                
            }
            catch(error){
            
                Handlinganerror( req , resp ,error);
            }
        
            break;
        }

        case "DELAY" : {
        
            try{
                setTimeout( function (){
                    
                    resp.writeHead( 200 , {'Content-Type' : 'text/html'});
                    resp.write("All good. Delayed by "+ url[ 3 ] +" Second.");
                    resp.end();
                
                } , url[ 3 ]*1000);
            }
            catch(error){
            
                Handlinganerror( req , resp ,error);
            }
        
            break;
        }

        
        case "STATUS" : {

            try{

                resp.writeHead( url[ 3 ] , {'Content-Type' : 'text/html'});
                resp.write(url[ 3 ]+" : "+ http.STATUS_CODES[ url[ 3 ] ] +"");
                resp.end();
            }
            catch(error){
            
                Handlinganerror( req , resp ,error);
            }

            break;
        }

        default : {

            resp.writeHead( 404 , {'Content-Type' : 'text/html',});            
            resp.write('404 not found');
            resp.end();
 
            break;
        }
    }
});


server.listen(port , ( error ) =>{
    if( error ){
    
        console.log(error);
        console.log("Error listening port" + port);
    } else {
    
        console.log("All good at port -" +port);
    }
});